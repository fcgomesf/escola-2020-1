package br.ucsal.bes20192.testequalidade.escola.business;

import br.ucsal.bes20192.testequalidade.escola.business.builders.*;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class AlunoBOIntegradoTest {

    /*
     * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
     * anos. Caso de teste # | entrada | saida esperada 1 | aluno nascido em 2003 |
     * * 16
     */
	
	@Test
    public void testarCalculoIdadeAluno1() {
        AlunoDAO alunoDAO = new AlunoDAO();
        alunoDAO.excluirTodos();
        Aluno aluno1 = new Aluno();
        AlunoBO alunoBO = new AlunoBO(alunoDAO, new DateHelper());
        aluno1 = AlunoBuilder.AlunoNascimento2003().Build();
        alunoDAO.salvar(aluno1);
        assertEquals(16, alunoBO.calcularIdade(123));

    }
    
    /*
     * Verificar se alunos ativos sao atualizados.
     */
    @Test
    
    public void testarAtualizacaoAlunosAtivos() {
        AlunoDAO alunoDAO = new AlunoDAO();
        alunoDAO.excluirTodos();

        AlunoBO alunoBo = new AlunoBO(alunoDAO, new DateHelper());
        Aluno alunoEsperado = AlunoBuilder.AlunoDefault().Build();

        alunoBo.atualizar(alunoEsperado);
        Aluno alunoAtual = alunoDAO.encontrarPorMatricula(alunoEsperado.getMatricula());
        assertEquals(alunoEsperado, alunoAtual);

    }

}