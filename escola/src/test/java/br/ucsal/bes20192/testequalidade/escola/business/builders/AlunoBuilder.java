package br.ucsal.bes20192.testequalidade.escola.business.builders;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {

	private static final Integer MATRICULA_DEFAULT = 123;

	private static final String NOME_DEFAULT = "CorongaVirus";

	private static final SituacaoAluno SITUACAO_DEFAULT = SituacaoAluno.ATIVO;

	private static final Integer AnoNascimento_DEFAULT = 2003;

	private Integer matricula;

	private String nome;

	private SituacaoAluno situacao;

	private Integer anoNascimento;

	public static AlunoBuilder AlunoDefault() {
		return AlunoBuilder.BuildarAluno().comMatricula(MATRICULA_DEFAULT).comNome(NOME_DEFAULT)
				.comSituacao(SITUACAO_DEFAULT).comAnoNascimentoInteger(AnoNascimento_DEFAULT);
	}

	public AlunoBuilder() {

	}

	public static AlunoBuilder BuildarAluno() {
		return new AlunoBuilder();
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public AlunoBuilder comSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
		return this;
	}

	public AlunoBuilder comAnoNascimentoInteger(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder comSituacaoAtivo() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}

	public AlunoBuilder comSituacaoCancelado() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}
	
	public static AlunoBuilder AlunoNascimento2003() {
		return AlunoBuilder.BuildarAluno().comMatricula(123).comAnoNascimentoInteger(2003);
	}

	public AlunoBuilder but() {
		return AlunoBuilder.BuildarAluno().comMatricula(this.matricula).comNome(this.nome).comSituacao(this.situacao)
				.comAnoNascimentoInteger(this.anoNascimento);
	}

	public AlunoBuilder(Integer matricula, String nome, SituacaoAluno situacao, Integer anoNascimento) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.situacao = situacao;
		this.anoNascimento = anoNascimento;
	}

	public Aluno Build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(this.matricula);
		aluno.setNome(this.nome);
		aluno.setSituacao(this.situacao);
		aluno.setAnoNascimento(this.anoNascimento);
		return aluno;
	}
}
